total 0
drwxrwxr-x    2 admin    root          1014 Nov  3  2023 bin
lrwxrwxrwx    1 admin    root            19 Nov  3  2023 cache -> /mnt/userdata/cache
drwxr-xr-x    3 admin    root             0 Jan  1 08:00 dev
drwxrwxr-x    3 admin    root           144 Nov  3  2023 etc
drwxrwxr-x    5 admin    root           414 Nov  3  2023 etc_ro
lrwxrwxrwx    1 admin    root            20 Nov  3  2023 etc_rw -> /mnt/userdata/etc_rw
drwxrwxr-x    3 admin    root          1060 Nov  3  2023 lib
drwxrwxr-x    2 admin    root             3 Nov  3  2023 media
drwxrwxr-x    5 admin    root            60 Nov  3  2023 mnt
dr-xr-xr-x  109 admin    root             0 Nov  3  1987 proc
drwxrwxr-x    2 admin    root          1501 Nov  3  2023 sbin
dr-xr-xr-x   18 admin    root             0 Jan  1  2000 sys
drwxr-xr-x    3 admin    root             0 Jan  1 08:00 tmp
drwxrwxr-x    4 admin    root            38 Nov  3  2023 usr
lrwxrwxrwx    1 admin    root            17 Nov  3  2023 var -> /mnt/userdata/var